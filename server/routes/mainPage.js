const express = require('express');

const router = express.Router();
const pool = require('../db/mysqlConf');
const { selectAll } = require('../db/sql');


const reg = /^[a-zA-Z]{1,10}$/g;

router.get('/', async (req, res) => {
  // fields contains extra meta data about results, if available
  // execute will internally call prepare and query
  const [rows, fields] = await pool.execute(selectAll);
  console.log(rows);

  res.setHeader('Content-Type', 'application/json');
  res.send(rows);
});

router.post('/', async (req, res) => {
  console.log(req.body);
  const keyword = req.body.keyword;
  console.log(keyword);
  const inputMatch = reg.test(keyword);
  reg .lastIndex = 0;
  if (inputMatch) {
    const select = `select * from mingxing where suoxie="${keyword}"`;
    const [rows, fields] = await pool.execute(select);
    res.send(rows);
    
  } else {
    res.send('请输入单个明星的首字母缩写');
  }
  
})

module.exports = router;
